export const GET_USER = "GET_USER";
export const SEARCH_USER = "SEARCH_USER";
export const DELETE_USER = "DELETE_USER";
export const GET_USER_BY_ID = "GET_USER_BY_ID";
export const PUT_USER_BY_ID = "PUT_USER_BY_ID";
export const POST_AVATAR = "POST_AVATAR";
