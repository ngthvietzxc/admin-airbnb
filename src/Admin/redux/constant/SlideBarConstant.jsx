import { FaUser, FaMapMarker, FaBed, FaBook } from "react-icons/fa";
import { AiOutlineMenu } from "react-icons/ai";
const sidebar_menu = [
  {
    id: 1,
    path: "/admin/dashboard",
    title: "DASHBOARD",
    icon: AiOutlineMenu,
  },
  {
    id: 2,
    path: "/admin/users",
    title: "Quản lý người dùng",
    icon: FaUser,
  },
  {
    id: 3,
    path: "/admin/locations",
    title: "Quản lý thông tin vị trí",
    icon: FaMapMarker,
  },
  {
    id: 4,
    path: "/admin/rooms",
    title: "Quản lý thông tin phòng",
    icon: FaBed,
  },
  {
    id: 5,
    path: "/admin/bookings",
    title: "Quản lý đặt phòng",
    icon: FaBook,
  },
];

export default sidebar_menu;
