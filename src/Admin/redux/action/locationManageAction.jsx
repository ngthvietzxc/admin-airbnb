import { message } from "antd";
import { locationManageService } from "../../services/locationManageService";
import {
  DELETE_LOCATION,
  GET_LOCATION,
  GET_LOCATION_BY_ID,
} from "./../constant/locationManageConstant";

export const getLocation = () => (dispatch) => {
  locationManageService
    .getLocation()
    .then((res) => {
      dispatch({
        type: GET_LOCATION,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const setAddLocation = (locationData) => {
  return (dispatch) => {
    locationManageService
      .addLocation(locationData)
      .then(() => {
        locationManageService.getLocation().then((res) => {
          dispatch({
            type: GET_LOCATION,
            payload: res.data.content,
          });
        });
        message.success("Thêm vị trí thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error("Thêm vị trí thất bại.");
      });
  };
};
export const deleteLocation = (id) => (dispatch) => {
  locationManageService
    .deleteLocation(id)
    .then(() => {
      dispatch({
        type: DELETE_LOCATION,
        payload: id,
      });
      message.success("Xóa vị trí thành công");
    })
    .catch((err) => {
      console.log(err);
      message.error("Xóa vị trí thất bại");
    });
};
export const getLocationById = (id) => (dispatch) => {
  locationManageService
    .getLocationById(id)
    .then((res) => {
      dispatch({
        type: GET_LOCATION_BY_ID,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const putLocationById = (id, locationDataById) => (dispatch) => {
  locationManageService
    .putLocationById(id, locationDataById)
    .then(() => {
      locationManageService.getLocationById(id).then((res) => {
        dispatch({
          type: GET_LOCATION_BY_ID,
          payload: res.data.content,
        });
      });
      message.success("Chỉnh sửa thông tin thành công");
    })
    .catch((err) => {
      console.log(err);
      message.error("Chỉnh sửa thông tin thất bại");
    });
};
