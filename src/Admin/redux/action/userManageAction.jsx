import { message } from "antd";
import { userManageService } from "../../services/userManageService";
import {
  DELETE_USER,
  GET_USER,
  GET_USER_BY_ID,
  POST_AVATAR,
  SEARCH_USER,
} from "../constant/userManageConstant";

export const getUser = () => (dispatch) => {
  userManageService
    .getUser()
    .then((res) => {
      dispatch({
        type: GET_USER,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const searchUser = (keyword) => (dispatch) => {
  userManageService
    .searchUser(keyword)
    .then((res) => {
      dispatch({
        type: SEARCH_USER,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const setAddUser = (userData) => {
  return (dispatch) => {
    userManageService
      .addUser(userData)
      .then(() => {
        userManageService.getUser().then((res) => {
          dispatch({
            type: GET_USER,
            payload: res.data.content,
          });
        });
        message.success("Thêm tài khoản thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error("Thêm tài khoản thất bại. Email đã tồn tại.");
      });
  };
};
export const deleteUser = (id) => (dispatch) => {
  userManageService
    .deleteUser(id)
    .then(() => {
      dispatch({
        type: DELETE_USER,
        payload: id,
      });
      message.success("Xóa tài khoản thành công");
    })
    .catch((err) => {
      console.log(err);
      message.success("Xóa tài khoản thất bại");
    });
};
export const getUserById = (id) => (dispatch) => {
  userManageService
    .getUserById(id)
    .then((res) => {
      dispatch({
        type: GET_USER_BY_ID,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const putUserById = (id, userDataById) => (dispatch) => {
  userManageService
    .putUserById(id, userDataById)
    .then(() => {
      userManageService.getUserById(id).then((res) => {
        dispatch({
          type: GET_USER_BY_ID,
          payload: res.data.content,
        });
      });
      message.success("Chỉnh sửa thông tin thành công");
    })
    .catch((err) => {
      console.log(err);
      message.error("Chỉnh sửa thông tin thất bại");
    });
};

export const postAvatar = (avatarFile) => (dispatch) => {
  userManageService
    .postAvatar(avatarFile)
    .then((res) => {
      dispatch({
        type: POST_AVATAR,
        payload: res.data,
      });
      message.success("Upload avatar thành công");
    })
    .catch((err) => {
      console.log(err);
      message.error("Upload avatar thất bại");
    });
};
