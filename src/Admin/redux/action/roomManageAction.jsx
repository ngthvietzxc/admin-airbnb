import { message } from "antd";
import { roomManageService } from "../../services/roomManageService";
import {
  DELETE_ROOM,
  GET_ROOM,
  GET_ROOM_BY_ID,
  POST_IMAGE_ROOM,
} from "./../constant/roomManageConstant";

export const getRoom = () => (dispatch) => {
  roomManageService
    .getRoom()
    .then((res) => {
      dispatch({
        type: GET_ROOM,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const setAddRoom = (roomData) => {
  return (dispatch) => {
    roomManageService
      .addRoom(roomData)
      .then(() => {
        roomManageService.getRoom().then((res) => {
          dispatch({
            type: GET_ROOM,
            payload: res.data.content,
          });
        });
        message.success("Thêm phòng thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error("Thêm phòng thất bại.");
      });
  };
};
export const deleteRoom = (id) => (dispatch) => {
  roomManageService
    .deleteRoom(id)
    .then(() => {
      dispatch({
        type: DELETE_ROOM,
        payload: id,
      });
      message.success("Xóa phòng thành công");
    })
    .catch((err) => {
      console.log(err);
      message.error("Xóa phòng thất bại");
    });
};
export const getRoomById = (id) => (dispatch) => {
  roomManageService
    .getRoomById(id)
    .then((res) => {
      dispatch({
        type: GET_ROOM_BY_ID,
        payload: res.data.content,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
export const putRoomById = (id, roomDataById) => (dispatch) => {
  roomManageService
    .putRoomById(id, roomDataById)
    .then(() => {
      roomManageService.getRoomById(id).then((res) => {
        dispatch({
          type: GET_ROOM_BY_ID,
          payload: res.data.content,
        });
      });
      message.success("Chỉnh sửa thông tin thành công");
    })
    .catch((err) => {
      console.log(err);
      message.error("Chỉnh sửa thông tin thất bại");
    });
};
export const postImageRoom = (id, roomFile) => (dispatch) => {
  roomManageService
    .postImageRoom(id, roomFile)
    .then((res) => {
      dispatch({
        type: POST_IMAGE_ROOM,
        payload: res.data,
      });
      message.success("Upload hình ảnh thành công");
    })
    .catch((err) => {
      console.log(err);
      message.error("Upload hình ảnh thất bại");
    });
};
