import { combineReducers } from "redux";
import userReducer from "./userReducer";
import { userManageReducer } from "./userManageReducer";
import { locationManageReducer } from "./locationManageReducer";
import { roomManageReducer } from "./roomManageReducer";
import { bookingManageReducer } from "./bookingManageReducer";

export const rootReducer = combineReducers({
  userReducer,
  userManageReducer,
  locationManageReducer,
  roomManageReducer,
  bookingManageReducer,
});
