import {
  GET_LOCATION,
  DELETE_LOCATION,
  PUT_LOCATION_BY_ID,
  GET_LOCATION_BY_ID,
} from "./../constant/locationManageConstant";
const initialState = {
  locationData: [],
  locationDataById: {},
};
export const locationManageReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LOCATION:
      return {
        ...state,
        locationData: action.payload,
      };
    case DELETE_LOCATION:
      return {
        ...state,
        locationData: state.locationData.filter(
          (location) => location.id !== action.payload
        ),
      };
    case GET_LOCATION_BY_ID:
      return {
        ...state,
        locationDataById: action.payload,
      };
    case PUT_LOCATION_BY_ID:
      return {
        ...state,
        locationData: action.payload,
      };
    default:
      return state;
  }
};
