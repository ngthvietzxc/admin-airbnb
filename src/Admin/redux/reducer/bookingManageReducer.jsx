import {
  DELETE_BOOKING,
  GET_BOOKING,
  GET_BOOKING_BY_ID,
  PUT_BOOKING_BY_ID,
} from "./../constant/bookingManageConstant";

const initialState = {
  bookingData: [],
  bookingDataById: {},
};
export const bookingManageReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_BOOKING:
      return {
        ...state,
        bookingData: action.payload,
      };
    case DELETE_BOOKING:
      return {
        ...state,
        bookingData: state.bookingData.filter(
          (booking) => booking.id !== action.payload
        ),
      };
    case GET_BOOKING_BY_ID:
      return {
        ...state,
        bookingDataById: action.payload,
      };
    case PUT_BOOKING_BY_ID:
      return {
        ...state,
        bookingData: action.payload,
      };
    default:
      return state;
  }
};
