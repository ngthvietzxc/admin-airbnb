import {
  DELETE_ROOM,
  GET_ROOM,
  GET_ROOM_BY_ID,
  POST_IMAGE_ROOM,
  PUT_ROOM_BY_ID,
} from "../constant/roomManageConstant";
const initialState = {
  roomData: [],
  roomDataById: {},
  image: null,
};
export const roomManageReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ROOM:
      return {
        ...state,
        roomData: action.payload,
      };
    case DELETE_ROOM:
      return {
        ...state,
        roomData: state.roomData.filter((room) => room.id !== action.payload),
      };
    case GET_ROOM_BY_ID:
      return {
        ...state,
        roomDataById: action.payload,
      };
    case PUT_ROOM_BY_ID:
      return {
        ...state,
        roomData: action.payload,
      };
    case POST_IMAGE_ROOM:
      return {
        ...state,
        image: action.payload,
      };
    default:
      return state;
  }
};
