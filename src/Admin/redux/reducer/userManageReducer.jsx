import {
  DELETE_USER,
  GET_USER,
  GET_USER_BY_ID,
  POST_AVATAR,
  PUT_USER_BY_ID,
  SEARCH_USER,
} from "../constant/userManageConstant";

const initialState = {
  userData: [],
  userDataById: {},
  avatar: null,
};

export const userManageReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      return {
        ...state,
        userData: action.payload,
      };
    case SEARCH_USER:
      return {
        ...state,
        userData: action.payload,
      };
    case DELETE_USER:
      return {
        ...state,
        userData: state.userData.filter((user) => user.id !== action.payload),
      };
    case GET_USER_BY_ID:
      return {
        ...state,
        userDataById: action.payload,
      };
    case PUT_USER_BY_ID:
      return {
        ...state,
        userData: action.payload,
      };
    case POST_AVATAR:
      return {
        ...state,
        avatar: action.payload,
      };
    default:
      return state;
  }
};
