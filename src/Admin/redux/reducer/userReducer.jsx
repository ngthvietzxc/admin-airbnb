import { localUserService } from "../../services/localService";
import { USER_LOGIN } from "./../constant/userConstant";

const initialState = {
  userInfo: localUserService.get(),
  userRegisterInfo: null,
  token: localUserService.get()?.token || null,
};

const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_LOGIN:
      const { token, ...userInfo } = payload;
      localUserService.set({ token });
      return {
        ...state,
        userInfo,
        token,
      };

    default:
      return state;
  }
};

export default userReducer;
