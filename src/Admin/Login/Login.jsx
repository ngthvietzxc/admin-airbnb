import { Button, Checkbox, Form, Input, message } from "antd";
import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { localUserService } from "../services/localService";
import { setUserLoginAction } from "../redux/action/userAction";
import { UserOutlined, LockOutlined } from "@ant-design/icons";

export default function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const onFinish = (values) => {
    const handleSuccess = (res) => {
      message.success("Đăng nhập thành công");
      localUserService.set(res.data.content);
      navigate(`/admin/dashboard`);
    };
    const handleError = (err) => {
      message.error(err);
    };

    dispatch(setUserLoginAction(values, handleSuccess, handleError));
  };
  return (
    <div className="min-h-screen bg-gradient-to-r from-purple-600 via-indigo-600 to-blue-600 flex items-center justify-center">
      <div className="max-w-md w-full bg-white p-8 rounded shadow">
        <h1 className="text-3xl font-bold mb-6 text-gray-800">Welcome Back</h1>
        <Form name="login-form" onFinish={onFinish}>
          <Form.Item
            name="email"
            rules={[{ required: true, message: "Please input your username!" }]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Username"
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item>
            <div className="flex justify-between">
              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox className="text-gray-700">Remember me</Checkbox>
              </Form.Item>
              <a className="login-form-forgot text-gray-700 " href="/">
                Forgot password?
              </a>
            </div>
          </Form.Item>
          <Form.Item>
            <Button
              type="primary"
              htmlType="submit"
              className="w-full bg-gradient-to-r from-purple-700 via-indigo-700 to-blue-700 hover:from-purple-800 hover:via-indigo-800 hover:to-blue-800"
            >
              Log in
            </Button>
          </Form.Item>
          <p className="text-center text-gray-500 text-sm">
            Don't have an account?
            <a className="text-blue-600 hover:underline" href="/">
              Register now!
            </a>
          </p>
        </Form>
      </div>
    </div>
  );
}
