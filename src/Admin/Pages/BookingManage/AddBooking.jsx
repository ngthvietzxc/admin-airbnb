import React from "react";
import { useDispatch } from "react-redux";
import { Form, Input } from "antd";
import { setAddBooking } from "../../redux/action/bookingManageAction";

export default function AddBooking({ onClose }) {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const onFinish = (values) => {
    const { maPhong, ngayDen, ngayDi, soLuongKhach, maNguoiDung } = values;

    const bookingData = {
      maPhong,
      ngayDen,
      ngayDi,
      soLuongKhach,
      maNguoiDung,
    };
    dispatch(setAddBooking(bookingData));
    form.resetFields();
    onClose();
  };

  const handleCancel = () => {
    form.resetFields();
    onClose();
  };

  return (
    <div className="container mx-auto" style={{ width: 450 }}>
      <div className="flex justify-end">
        <button
          className="text-gray-600 hover:text-gray-800"
          onClick={handleCancel}
        >
          <svg
            className="w-4 h-4 fill-current"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <path
              fillRule="evenodd"
              d="M11.414 10l4.293-4.293a1 1 0 1 0-1.414-1.414L10 8.586 5.707 4.293a1 1 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a1 1 0 1 0 1.414 1.414L10 11.414l4.293 4.293a1 1 0 1 0 1.414-1.414L11.414 10z"
              clipRule="evenodd"
            />
          </svg>
        </button>
      </div>
      <h2 className="text-center text-2xl font-bold mb-5">Thêm đặt phòng</h2>

      <Form
        form={form}
        name="basic"
        labelCol={{
          span: 8,
        }}
        wrapperCol={{
          span: 16,
        }}
        className="max-w-md mx-auto"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="Mã Phòng"
          name="maPhong"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập mã phòng!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Ngày Đến"
          name="ngayDen"
          rules={[
            {
              required: true,
              message: "Vui lòng chọn ngày đến!",
            },
          ]}
        >
          <Input type="date" />
        </Form.Item>
        <Form.Item
          label="Ngày Đi"
          name="ngayDi"
          rules={[
            {
              required: true,
              message: "Vui lòng chọn ngày đi!",
            },
          ]}
        >
          <Input type="date" />
        </Form.Item>
        <Form.Item
          label="Số Lượng Khách"
          name="soLuongKhach"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập số lượng khách!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Mã Người Dùng"
          name="maNguoiDung"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập mã người dùng!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <button
            type="primary"
            htmlType="submit"
            className="  hover:bg-blue-800 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
            style={{ backgroundColor: "#35425e" }}
          >
            <span className="mx-auto">Submit</span>
          </button>
        </Form.Item>
      </Form>
    </div>
  );
}
