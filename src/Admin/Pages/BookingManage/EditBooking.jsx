import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Input } from "antd";
import { useParams } from "react-router-dom";
import {
  getBookingById,
  putBookingById,
} from "../../redux/action/bookingManageAction";
import moment from "moment";
import { getUserById } from "../../redux/action/userManageAction";
import { getRoomById } from "../../redux/action/roomManageAction";

export default function EditBooking() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const booking = useSelector(
    (state) => state.bookingManageReducer.bookingDataById
  );
  const room = useSelector((state) => state.roomManageReducer.roomDataById);
  const user = useSelector((state) => state.userManageReducer.userDataById);

  const [maPhong, setMaPhong] = useState("");
  const [ngayDen, setNgayDen] = useState("");
  const [ngayDi, setNgayDi] = useState("");
  const [soLuongKhach, setSoLuongKhach] = useState(null);
  const [maNguoiDung, setMaNguoiDung] = useState("");

  useEffect(() => {
    dispatch(getBookingById(parseInt(id)));
  }, [dispatch, id]);
  useEffect(() => {
    dispatch(getRoomById(booking.maPhong));
  }, [dispatch, booking.maPhong]);
  useEffect(() => {
    dispatch(getUserById(booking.maNguoiDung));
  }, [dispatch, booking.maNguoiDung]);
  useEffect(() => {
    if (booking) {
      setMaPhong(booking.maPhong);
      setNgayDen(
        booking.ngayDen ? moment(booking.ngayDen).format("YYYY-MM-DD") : ""
      );
      setNgayDi(
        booking.ngayDi ? moment(booking.ngayDi).format("YYYY-MM-DD") : ""
      );
      setSoLuongKhach(booking.soLuongKhach);
      setMaNguoiDung(booking.maNguoiDung);
    }
  }, [booking]);
  const handleSaveBooking = () => {
    const updatedBookingData = {
      maPhong,
      ngayDen,
      ngayDi,
      soLuongKhach,
      maNguoiDung,
    };
    dispatch(putBookingById(parseInt(id), updatedBookingData));
    setEditingMode(false);
  };
  const [editingMode, setEditingMode] = useState(false);
  const renderEdit = () => (
    <div className="fixed inset-0 flex items-center justify-center bg-opacity-75 bg-gray-500">
      <div className="bg-white p-8 rounded " style={{ width: 500 }}>
        <div className="flex w-full justify-center">
          <h2 className="font-bold text-xl pb-5">Thay đổi thông tin</h2>
        </div>

        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          onFinish={handleSaveBooking}
        >
          <Form.Item label="Mã Phòng">
            <Input
              value={maPhong}
              onChange={(e) => setMaPhong(e.target.value)}
            />
          </Form.Item>

          <Form.Item label="Ngày Đến">
            <Input
              type="date"
              value={ngayDen}
              onChange={(e) => setNgayDen(e.target.value)}
            />
          </Form.Item>
          <Form.Item label="Ngày Đi">
            <Input
              type="date"
              value={ngayDi}
              onChange={(e) => setNgayDi(e.target.value)}
            />
          </Form.Item>

          <Form.Item label="Số Lượng Khách">
            <Input
              value={soLuongKhach}
              onChange={(e) => setSoLuongKhach(e.target.value)}
            />
          </Form.Item>
          <Form.Item label="Mã Người Dùng">
            <Input
              value={maNguoiDung}
              onChange={(e) => setMaNguoiDung(e.target.value)}
            />
          </Form.Item>
          <Form.Item className="flex justify-end">
            <div className="flex space-x-2">
              <div>
                <button
                  type="primary"
                  htmlType="submit"
                  className="    text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
                  style={{ backgroundColor: "#35425e" }}
                >
                  <span className="mx-auto">Lưu</span>
                </button>
              </div>
              <div>
                <button
                  type="primary"
                  className="   text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
                  style={{ backgroundColor: "#35425e" }}
                  onClick={() => setEditingMode(false)}
                >
                  Hủy
                </button>
              </div>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
  return (
    <div className="container mx-auto pt-5">
      <h1 className="pl-5 pb-5 text-3xl font-bold">
        Thông tin chi tiết đặt phòng (ID: {booking.id})
      </h1>
      <div className="px-10 py-10 m-5 border-solid border-2 border-black bg-gray-300">
        <div className="flex">
          <div className="w-1/3 space-y-5 pr-10">
            <div className="w-full h-80 bg-white rounded flex flex-col  px-10 pt-5  shadow">
              <h3 className="text-lg font-semibold mb-2 flex justify-center">
                Thông tin đặt phòng
              </h3>
              <div className="flex flex-col h-48 justify-evenly ">
                <p>
                  <span className="font-semibold">Ngày đến:</span>
                  {booking.ngayDen
                    ? moment(booking.ngayDen).format("DD-MM-YYYY")
                    : ""}
                </p>
                <p>
                  <span className="font-semibold">Ngày đi:</span>
                  {booking.ngayDi
                    ? moment(booking.ngayDi).format("DD-MM-YYYY")
                    : ""}
                </p>
                <p>
                  <span className="font-semibold">Số lượng khách:</span>
                  {booking.soLuongKhach}
                </p>
                <p>
                  <span className="font-semibold">Mã phòng:</span>{" "}
                  {booking.maPhong}
                </p>
                <p>
                  <span className="font-semibold">Mã người đặt phòng:</span>
                  {booking.maNguoiDung}
                </p>
              </div>
              {!editingMode && (
                <Form.Item wrapperCol={{}}>
                  <button
                    type="primary"
                    className="  hover:bg-blue-800 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
                    style={{ backgroundColor: "#35425e" }}
                    onClick={() => setEditingMode(true)}
                  >
                    <span className="mx-auto">Chỉnh sửa</span>
                  </button>
                </Form.Item>
              )}
              {editingMode && renderEdit()}
            </div>
            <div className="w-full h-80 bg-white rounded flex flex-col px-10  pt-5 shadow">
              <h3 className="text-lg font-semibold mb-2 flex justify-center">
                Thông tin người đặt phòng
              </h3>
              <div className="h-20 flex  flex-col justify-evenly">
                <p>
                  <span className="font-semibold">Tên người dùng:</span>{" "}
                  {user.name}
                </p>
                <p>
                  <span className="font-semibold">Email:</span> {user.email}
                </p>
              </div>
            </div>
          </div>
          <div className="w-2/3 ">
            <div className="w-full h-full bg-white rounded flex flex-col px-10  pt-5 shadow">
              <h3 className="text-lg font-semibold mb-2 flex justify-center">
                Thông tin phòng
              </h3>
              <div className="flex flex-col justify-evenly h-48">
                <p>
                  <span className="font-semibold">Tên phòng:</span>{" "}
                  {room.tenPhong}
                </p>
                <p>
                  <span className="font-semibold">Mô tả:</span> {room.moTa}
                </p>
              </div>
              <div>
                <img src={room.hinhAnh} alt="" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
