import React from "react";

export default function Dashboard() {
  return (
    <div className="bg-cover bg-center h-screen">
      <p className=" text-4xl flex font-bold  justify-center h-full animate-bounce h-4 pt-48 ">
        ADMIN SYSTEM
      </p>
    </div>
  );
}
