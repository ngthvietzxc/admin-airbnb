import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Checkbox, Form, Input } from "antd";
import { useParams } from "react-router-dom";
import { getRoomById, putRoomById } from "../../redux/action/roomManageAction";

export default function EditRoom() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const room = useSelector((state) => state.roomManageReducer.roomDataById);

  const [tenPhong, setTenPhong] = useState("");
  const [khach, setKhach] = useState(0);
  const [phongNgu, setPhongNgu] = useState(0);
  const [giuong, setGiuong] = useState(0);
  const [phongTam, setPhongTam] = useState(0);
  const [moTa, setMoTa] = useState("");
  const [giaTien, setGiaTien] = useState(0);
  const [mayGiat, setMayGiat] = useState(false);
  const [banLa, setBanLa] = useState(false);
  const [tivi, setTivi] = useState(false);
  const [dieuHoa, setDieuHoa] = useState(false);
  const [wifi, setWifi] = useState(false);
  const [bep, setBep] = useState(false);
  const [doXe, setDoXe] = useState(false);
  const [hoBoi, setHoBoi] = useState(false);
  const [banUi, setBanUi] = useState(false);
  const [maViTri, setMaViTri] = useState(0);
  const [hinhAnh, setHinhAnh] = useState("");

  useEffect(() => {
    dispatch(getRoomById(parseInt(id)));
  }, [dispatch, id]);

  useEffect(() => {
    if (room) {
      setTenPhong(room.tenPhong);
      setKhach(room.khach);
      setPhongNgu(room.phongNgu);
      setGiuong(room.giuong);
      setPhongTam(room.phongTam);
      setMoTa(room.moTa);
      setGiaTien(room.giaTien);
      setMayGiat(room.mayGiat);
      setBanLa(room.banLa);
      setTivi(room.tivi);
      setDieuHoa(room.dieuHoa);
      setWifi(room.wifi);
      setBep(room.bep);
      setDoXe(room.doXe);
      setHoBoi(room.hoBoi);
      setBanUi(room.banUi);
      setMaViTri(room.maViTri);
      setHinhAnh(room.hinhAnh);
    }
  }, [room]);

  const handleSaveRoom = () => {
    const updatedRoomData = {
      tenPhong,
      khach,
      phongNgu,
      giuong,
      phongTam,
      moTa,
      giaTien,
      mayGiat,
      banLa,
      tivi,
      dieuHoa,
      wifi,
      bep,
      doXe,
      hoBoi,
      banUi,
      maViTri,
      hinhAnh,
    };
    dispatch(putRoomById(parseInt(id), updatedRoomData));
    setEditingMode(false);
  };

  const [editingMode, setEditingMode] = useState(false);
  const renderEdit = () => (
    <div className="fixed inset-0 flex items-center justify-center bg-opacity-75 bg-gray-500">
      <div className="bg-white p-8 rounded " style={{ width: 500 }}>
        <div className="flex w-full justify-center">
          <h2 className="font-bold text-xl pb-5">Thay đổi thông tin</h2>
        </div>

        <Form
          name="basic"
          labelCol={{
            span: 16,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          onFinish={handleSaveRoom}
        >
          <div className="flex">
            <div>
              <Form.Item label="Tên Phòng">
                <Input
                  value={tenPhong}
                  onChange={(e) => setTenPhong(e.target.value)}
                />
              </Form.Item>
              <Form.Item label="Khách">
                <Input
                  value={khach}
                  onChange={(e) => setKhach(parseInt(e.target.value))}
                />
              </Form.Item>
              <Form.Item label="Phòng Ngủ">
                <Input
                  value={phongNgu}
                  onChange={(e) => setPhongNgu(parseInt(e.target.value))}
                />
              </Form.Item>
              <Form.Item label="Giường">
                <Input
                  value={giuong}
                  onChange={(e) => setGiuong(parseInt(e.target.value))}
                />
              </Form.Item>
              <Form.Item label="Phòng Tắm">
                <Input
                  value={phongTam}
                  onChange={(e) => setPhongTam(parseInt(e.target.value))}
                />
              </Form.Item>
              <Form.Item label="Mô Tả">
                <Input value={moTa} onChange={(e) => setMoTa(e.target.value)} />
              </Form.Item>
              <Form.Item label="Giá Tiền">
                <Input
                  value={giaTien}
                  onChange={(e) => setGiaTien(parseInt(e.target.value))}
                />
              </Form.Item>
              <Form.Item label="Mã Vị Trí">
                <Input
                  value={maViTri}
                  onChange={(e) => setMaViTri(parseInt(e.target.value))}
                />
              </Form.Item>
              <Form.Item label="Hình Ảnh">
                <Input
                  value={hinhAnh}
                  onChange={(e) => setHinhAnh(e.target.value)}
                />
              </Form.Item>
            </div>
            <div>
              <Form.Item label="Máy Giặt">
                <Checkbox
                  checked={mayGiat}
                  onChange={(e) => setMayGiat(e.target.checked)}
                />
              </Form.Item>
              <Form.Item label="Bàn Là">
                <Checkbox
                  checked={banLa}
                  onChange={(e) => setBanLa(e.target.checked)}
                />
              </Form.Item>
              <Form.Item label="TiVi">
                <Checkbox
                  checked={tivi}
                  onChange={(e) => setTivi(e.target.checked)}
                />
              </Form.Item>
              <Form.Item label="Điều Hòa">
                <Checkbox
                  checked={dieuHoa}
                  onChange={(e) => setDieuHoa(e.target.checked)}
                />
              </Form.Item>
              <Form.Item label="Wifi">
                <Checkbox
                  checked={wifi}
                  onChange={(e) => setWifi(e.target.checked)}
                />
              </Form.Item>
              <Form.Item label="Bếp">
                <Checkbox
                  checked={bep}
                  onChange={(e) => setBep(e.target.checked)}
                />
              </Form.Item>
              <Form.Item label="Đỗ Xe">
                <Checkbox
                  checked={doXe}
                  onChange={(e) => setDoXe(e.target.checked)}
                />
              </Form.Item>
              <Form.Item label="Hồ Bơi">
                <Checkbox
                  checked={hoBoi}
                  onChange={(e) => setHoBoi(e.target.checked)}
                />
              </Form.Item>
              <Form.Item label="Bàn Ủi">
                <Checkbox
                  checked={banUi}
                  onChange={(e) => setBanUi(e.target.checked)}
                />
              </Form.Item>
            </div>
          </div>
          <Form.Item className="flex justify-end">
            <div className="flex space-x-2">
              <div>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="bg-gradient-to-r from-blue-500 to-purple-500 hover:from-blue-600 hover:to-purple-600 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
                >
                  <span className="mx-auto">Save</span>
                </Button>
              </div>
              <div>
                <Button
                  type="primary"
                  className="bg-gradient-to-r from-blue-500 to-purple-500 hover:from-blue-600 hover:to-purple-600 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
                  onClick={() => setEditingMode(false)}
                >
                  Cancel
                </Button>
              </div>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
  return (
    <div className="container mx-auto pt-5">
      <h1 className="pl-5 pb-5 text-3xl font-bold">
        Thông tin chi tiết phòng (ID: {room.id})
      </h1>
      <div className="bg-gray-100 rounded-lg p-6">
        <div className="grid grid-cols-2 gap-4 mb-6">
          <div>
            <h2 className="text-2xl font-bold mb-2">{room.tenPhong}</h2>
            <p className="text-sm">
              <span className="font-semibold">Khách:</span> {room.khach}
            </p>
            <p className="text-sm">
              <span className="font-semibold">Phòng Ngủ:</span> {room.phongNgu}
            </p>
            <p className="text-sm">
              <span className="font-semibold">Giường:</span> {room.giuong}
            </p>
            <p className="text-sm">
              <span className="font-semibold">Phòng Tắm:</span> {room.phongTam}
            </p>
            <p className="text-sm">
              <span className="font-semibold">Mô Tả:</span> {room.moTa}
            </p>
          </div>
          <div className="flex flex-col">
            <div className="mb-4">
              <span className="font-semibold">Tiện Nghi:</span>
              <p className="text-sm">
                {room.mayGiat ? "Máy Giặt" : null}
                {room.banLa ? ", Bàn Là" : null}
                {room.tivi ? ", TiVi" : null}
                {room.dieuHoa ? ", Điều Hòa" : null}
                {room.wifi ? ", Wifi" : null}
                {room.bep ? ", Bếp" : null}
                {room.doXe ? ", Đỗ Xe" : null}
                {room.hoBoi ? ", Hồ Bơi" : null}
                {room.banUi ? ", Bàn Ủi" : null}
              </p>
            </div>
            <p className="text-sm">
              <span className="font-semibold">Giá Tiền:</span> {room.giaTien}
            </p>
            <p className="text-sm">
              <span className="font-semibold">Mã Vị Trí:</span> {room.maViTri}
            </p>
          </div>
        </div>
        <div className="flex justify-between items-center mb-6">
          {!editingMode ? (
            <button
              className="bg-blue-500 hover:bg-blue-600 text-white font-semibold rounded px-4 py-2"
              onClick={() => setEditingMode(true)}
            >
              Chỉnh sửa
            </button>
          ) : (
            renderEdit()
          )}
        </div>
        <img className="w-full mb-4" src={room.hinhAnh} alt="" />
      </div>
    </div>
  );
}
