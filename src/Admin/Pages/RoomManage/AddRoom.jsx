import React from "react";
import { useDispatch } from "react-redux";
import { Button, Form, Input, Checkbox } from "antd";
import { setAddRoom } from "../../redux/action/roomManageAction";

export default function AddRoom({ onClose }) {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const onFinish = (values) => {
    const {
      tenPhong,
      khach,
      phongNgu,
      giuong,
      phongTam,
      moTa,
      giaTien,
      mayGiat,
      banLa,
      tivi,
      dieuHoa,
      wifi,
      bep,
      doXe,
      hoBoi,
      banUi,
      maViTri,
      hinhAnh,
    } = values;

    const roomData = {
      tenPhong,
      khach: parseInt(khach),
      phongNgu: parseInt(phongNgu),
      giuong: parseInt(giuong),
      phongTam: parseInt(phongTam),
      moTa,
      giaTien: parseInt(giaTien),
      mayGiat,
      banLa,
      tivi,
      dieuHoa,
      wifi,
      bep,
      doXe,
      hoBoi,
      banUi,
      maViTri: parseInt(maViTri),
      hinhAnh,
    };
    dispatch(setAddRoom(roomData));
    form.resetFields();
    onClose();
  };

  const handleCancel = () => {
    form.resetFields();
    onClose();
  };

  return (
    <div className="container " style={{ width: 600 }}>
      <div className="flex justify-end">
        <button
          className="text-gray-600 hover:text-gray-800"
          onClick={handleCancel}
        >
          <svg
            className="w-4 h-4 fill-current"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <path
              fillRule="evenodd"
              d="M11.414 10l4.293-4.293a1 1 0 1 0-1.414-1.414L10 8.586 5.707 4.293a1 1 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a1 1 0 1 0 1.414 1.414L10 11.414l4.293 4.293a1 1 0 1 0 1.414-1.414L11.414 10z"
              clipRule="evenodd"
            />
          </svg>
        </button>
      </div>
      <h2 className="text-center text-2xl font-bold mb-5">Thêm vị trí</h2>
      <Form
        form={form}
        name="basic"
        className="max-w-md mx-auto"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <div className="flex space-x-8">
          <div>
            <Form.Item
              label="Tên phòng"
              name="tenPhong"
              rules={[
                {
                  required: true,
                  message: "Vui lòng điền tên phòng!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Khách"
              name="khach"
              rules={[
                {
                  required: true,
                  message: "Vui lòng điền tỉnh thành!",
                },
              ]}
            >
              <Input type="number" />
            </Form.Item>
            <Form.Item
              label="Phòng Ngủ"
              name="phongNgu"
              rules={[
                {
                  required: true,
                  message: "Vui lòng điền quốc gia!",
                },
              ]}
            >
              <Input type="number" />
            </Form.Item>
            <Form.Item
              label="Giường"
              name="giuong"
              rules={[
                {
                  required: true,
                  message: "Vui lòng điền quốc gia!",
                },
              ]}
            >
              <Input type="number" />
            </Form.Item>
            <Form.Item
              label="Phòng Tắm"
              name="phongTam"
              rules={[
                {
                  required: true,
                  message: "Vui lòng điền quốc gia!",
                },
              ]}
            >
              <Input type="number" />
            </Form.Item>
            <Form.Item
              label="Mô tả"
              name="moTa"
              rules={[
                {
                  required: true,
                  message: "Vui lòng điền quốc gia!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Giá tiền"
              name="giaTien"
              rules={[
                {
                  required: true,
                  message: "Vui lòng điền quốc gia!",
                },
              ]}
            >
              <Input type="number" />
            </Form.Item>
            <Form.Item
              label="Mã vị trí"
              name="maViTri"
              rules={[
                {
                  required: true,
                  message: "Vui lòng điền quốc gia!",
                },
              ]}
            >
              <Input type="number" />
            </Form.Item>
            <Form.Item
              label="Hình Ảnh"
              name="hinhAnh"
              rules={[
                {
                  required: true,
                  message: "Vui lòng thêm hình ảnh",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </div>
          <div>
            <Form.Item
              label="Bàn là"
              name="banLa"
              valuePropName="checked"
              initialValue={false}
            >
              <Checkbox />
            </Form.Item>
            <Form.Item
              label="Máy giặt"
              name="mayGiat"
              valuePropName="checked"
              initialValue={false}
            >
              <Checkbox />
            </Form.Item>
            <Form.Item
              label="Ti Vi"
              name="tivi"
              valuePropName="checked"
              initialValue={false}
            >
              <Checkbox />
            </Form.Item>
            <Form.Item
              label="Điều hòa"
              name="dieuHoa"
              valuePropName="checked"
              initialValue={false}
            >
              <Checkbox />
            </Form.Item>
            <Form.Item
              label="Wifi"
              name="wifi"
              valuePropName="checked"
              initialValue={false}
            >
              <Checkbox />
            </Form.Item>
            <Form.Item
              label="Bếp"
              name="bep"
              valuePropName="checked"
              initialValue={false}
            >
              <Checkbox />
            </Form.Item>
            <Form.Item
              label="Đỗ xe"
              name="doXe"
              valuePropName="checked"
              initialValue={false}
            >
              <Checkbox />
            </Form.Item>
            <Form.Item
              label="Hồ bơi"
              name="hoBoi"
              valuePropName="checked"
              initialValue={false}
            >
              <Checkbox />
            </Form.Item>
            <Form.Item
              label="Bàn ủi"
              name="banUi"
              valuePropName="checked"
              initialValue={false}
            >
              <Checkbox />
            </Form.Item>
          </div>
        </div>
        <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
          <Button
            type="primary"
            htmlType="submit"
            className="bg-gradient-to-r from-blue-500 to-purple-500 hover:from-blue-600 hover:to-purple-600 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
          >
            <span className="mx-auto">Submit</span>
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}
