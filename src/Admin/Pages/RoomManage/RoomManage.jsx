import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteRoom, getRoom } from "./../../redux/action/roomManageAction";
import { NavLink } from "react-router-dom";
import { FiTrash2 } from "react-icons/fi";
import { Button, Card } from "antd";
import Pagination from "@mui/material/Pagination";
import AddRoom from "./AddRoom";

export default function RoomManage() {
  const dispatch = useDispatch();
  const rooms = useSelector((state) => state.roomManageReducer.roomData);
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 4;
  const totalPages = Math.ceil(rooms.length / itemsPerPage);

  useEffect(() => {
    dispatch(getRoom());
  }, [dispatch]);

  const handleDeleteRoom = (id) => {
    dispatch(deleteRoom(id));
  };

  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };

  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const currentPageRooms = [...rooms].slice(startIndex, endIndex);
  // Add Room
  const [showAddRoom, setShowAddRoom] = useState(false);
  const handleAddRoomClick = () => {
    setShowAddRoom(!showAddRoom);
  };
  return (
    <div className="container mx-auto pt-5">
      <h1 className="pl-5 pb-5 text-3xl font-bold ">Quản lý thông tin phòng</h1>
      <div className="px-10 py-5 m-5 border-solid border-2 border-black bg-gray-300">
        <div className="flex pb-5 justify-end">
          <div className="w-48">
            <Button
              type="primary"
              htmlType="submit"
              className="   hover:bg-blue-800 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
              style={{ backgroundColor: "#35425e" }}
              onClick={handleAddRoomClick}
            >
              <span className="mx-auto">Thêm phòng</span>
            </Button>
          </div>
        </div>
        <p className="text-center pb-5 text-gray-800 text-xl font-bold">
          DANH SÁCH THÔNG TIN PHÒNG
        </p>
        <div className="grid grid-cols-1  grid-cols-2 gap-10  ">
          {currentPageRooms.map((room) => (
            <Card
              key={room.id}
              className="w-full shadow hover:border-dashed hover:border-black"
              cover={
                <img
                  className="h-96 cursor-pointer w-1/2 object-scale-down h-auto"
                  src={room.hinhAnh}
                  alt=""
                />
              }
              actions={[
                <div key="detail" className="flex">
                  <NavLink
                    to={`/admin/rooms/${room.id}`}
                    type="warning"
                    className="bg-white text-black font-semibold text-xs rounded px-4 py-2 w-full flex justify-center items-center"
                  >
                    <span className="mx-auto">Xem thông tin chi tiết</span>
                  </NavLink>
                </div>,
                <div className="justify-end flex pr-5">
                  <Button
                    key="delete"
                    type="warning"
                    htmlType="submit"
                    className="bg-red-500 text-black  text-xs rounded px-4 py-2 w-full flex justify-center items-center"
                    icon={<FiTrash2 />}
                    onClick={() => handleDeleteRoom(room.id)}
                  ></Button>
                </div>,
              ]}
            >
              <Card.Meta title={room.tenPhong} />
            </Card>
          ))}
        </div>
        <div className="flex justify-center p-10">
          <Pagination
            color="primary"
            count={totalPages}
            page={currentPage}
            onChange={handlePageChange}
          />
        </div>
      </div>
      {showAddRoom && (
        <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
          <div className="bg-white p-8 rounded-lg">
            <AddRoom onClose={handleAddRoomClick} />
          </div>
        </div>
      )}
    </div>
  );
}
