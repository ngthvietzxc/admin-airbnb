import React from "react";
import { useDispatch } from "react-redux";
import { Form, Input } from "antd";

import { setAddLocation } from "./../../redux/action/locationManageAction";

export default function AddLocation({ onClose }) {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const onFinish = (values) => {
    const { tenViTri, tinhThanh, quocGia, hinhAnh } = values;

    const locationData = {
      tenViTri,
      tinhThanh,
      quocGia,
      hinhAnh,
    };
    dispatch(setAddLocation(locationData));
    form.resetFields();
    onClose();
  };

  const handleCancel = () => {
    form.resetFields();
    onClose();
  };

  return (
    <div className="container mx-auto" style={{ width: 450 }}>
      <div className="flex justify-end">
        <button
          className="text-gray-600 hover:text-gray-800"
          onClick={handleCancel}
        >
          <svg
            className="w-4 h-4 fill-current"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <path
              fillRule="evenodd"
              d="M11.414 10l4.293-4.293a1 1 0 1 0-1.414-1.414L10 8.586 5.707 4.293a1 1 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a1 1 0 1 0 1.414 1.414L10 11.414l4.293 4.293a1 1 0 1 0 1.414-1.414L11.414 10z"
              clipRule="evenodd"
            />
          </svg>
        </button>
      </div>
      <h2 className="text-center text-2xl font-bold mb-5">Thêm vị trí</h2>

      <Form
        form={form}
        name="basic"
        labelCol={{
          span: 6,
        }}
        wrapperCol={{
          span: 16,
        }}
        className="max-w-md mx-auto"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="Tên vị trí"
          name="tenViTri"
          rules={[
            {
              required: true,
              message: "Vui lòng điền tên vị trí!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Tỉnh Thành"
          name="tinhThanh"
          rules={[
            {
              required: true,
              message: "Vui lòng điền tỉnh thành!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Quốc Gia"
          name="quocGia"
          rules={[
            {
              required: true,
              message: "Vui lòng điền quốc gia!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Hình Ảnh"
          name="hinhAnh"
          rules={[
            {
              required: true,
              message: "Vui lòng thêm hình ảnh",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 6, span: 16 }}>
          <button
            type="primary"
            htmlType="submit"
            className="  hover:bg-blue-800 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
            style={{ backgroundColor: "#35425e" }}
          >
            <span className="mx-auto">Submit</span>
          </button>
        </Form.Item>
      </Form>
    </div>
  );
}
