import { Button, Card, Modal } from "antd";
import { FiTrash2 } from "react-icons/fi";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import {
  deleteLocation,
  getLocation,
} from "./../../redux/action/locationManageAction";
import AddLocation from "./AddLocation";
import { Pagination } from "@mui/material";

export default function LocationManage() {
  const dispatch = useDispatch();
  const locations = useSelector(
    (state) => state.locationManageReducer.locationData
  );
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedImage, setSelectedImage] = useState("");

  useEffect(() => {
    dispatch(getLocation());
  }, [dispatch]);

  const handleImageClick = (image) => {
    setSelectedImage(image);
    setModalVisible(true);
  };

  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 4;
  const totalPages = Math.ceil(locations.length / itemsPerPage);
  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };
  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const currentPageLocations = [...locations]
    .reverse()
    .slice(startIndex, endIndex);

  // Add Location
  const [showAddLocation, setShowAddLocation] = useState(false);
  const handleAddLocationClick = () => {
    setShowAddLocation(!showAddLocation);
  };
  //   Delete Location
  const handleDeleteLocation = (id) => {
    dispatch(deleteLocation(id));
  };
  return (
    <div className="container mx-auto pt-5">
      <h1 className="pl-5 pb-5 text-3xl font-bold ">
        Quản lý thông tin vị trí
      </h1>
      <div className="px-10 py-5 m-5 border-solid border-2 border-black bg-gray-300">
        <div className="flex pb-5 justify-end">
          <div className="w-48">
            <Button
              type="primary"
              htmlType="submit"
              className="   hover:bg-blue-800 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
              style={{ backgroundColor: "#35425e" }}
              onClick={handleAddLocationClick}
            >
              <span className="mx-auto">Thêm vị trí</span>
            </Button>
          </div>
        </div>
        <p className="text-center pb-5 text-gray-800 text-xl font-bold">
          DANH SÁCH THÔNG TIN VỊ TRÍ
        </p>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-10  ">
          {currentPageLocations.map((location) => (
            <Card
              key={location.id}
              className="w-full shadow hover:border-dashed hover:border-black"
              cover={
                <img
                  className="h-64 cursor-pointer "
                  src={location.hinhAnh}
                  alt=""
                  onClick={() => handleImageClick(location.hinhAnh)}
                />
              }
              actions={[
                <div key="detail" className="flex">
                  <NavLink
                    to={`/admin/locations/${location.id}`}
                    type="warning"
                    className="bg-white text-black font-semibold text-xs rounded px-4 py-2 w-full flex justify-center items-center"
                  >
                    <span className="mx-auto">Xem thông tin chi tiết</span>
                  </NavLink>
                </div>,
                <div className="justify-end flex pr-5 ">
                  <div className="w-10 h-8">
                    <button
                      className="bg-red-600 text-white text-lg justify-center flex rounded h-full w-full flex  items-center"
                      onClick={() => handleDeleteLocation(location.id)}
                    >
                      <FiTrash2 />
                    </button>
                  </div>
                </div>,
              ]}
            >
              <Card.Meta
                title={location.tenViTri}
                description={
                  <>
                    <div>ID: {location.id}</div>
                    <div>Tỉnh Thành: {location.tinhThanh}</div>
                    <div>Quốc Gia: {location.quocGia}</div>
                  </>
                }
              />
            </Card>
          ))}
        </div>
        <div className="flex justify-center pt-7 pb-3">
          <Pagination
            variant="outlined"
            color="primary"
            count={totalPages}
            page={currentPage}
            onChange={handlePageChange}
          />
        </div>
      </div>

      <Modal
        open={modalVisible}
        onCancel={() => setModalVisible(false)}
        footer={null}
      >
        <img src={selectedImage} alt="" style={{ width: "100%" }} />
      </Modal>
      {showAddLocation && (
        <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
          <div className="bg-white p-8 rounded-lg">
            <AddLocation onClose={handleAddLocationClick} />
          </div>
        </div>
      )}
    </div>
  );
}
