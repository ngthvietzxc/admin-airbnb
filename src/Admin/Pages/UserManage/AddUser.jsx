import React from "react";
import { useDispatch } from "react-redux";
import { Form, Input, Select } from "antd";

import { setAddUser } from "./../../redux/action/userManageAction";

export default function AddUser({ onClose }) {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const onFinish = (values) => {
    const { email, password, name, role, gender, birthday } = values;

    const userData = {
      role,
      email,
      password,
      name,
      gender: gender === "true",
      birthday,
    };
    dispatch(setAddUser(userData));
    form.resetFields();
    onClose();
  };

  const handleCancel = () => {
    form.resetFields();
    onClose();
  };

  return (
    <div className="container mx-auto" style={{ width: 450 }}>
      <div className="flex justify-end">
        <button
          className="text-gray-600 hover:text-gray-800"
          onClick={handleCancel}
        >
          <svg
            className="w-4 h-4 fill-current"
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 20 20"
          >
            <path
              fillRule="evenodd"
              d="M11.414 10l4.293-4.293a1 1 0 1 0-1.414-1.414L10 8.586 5.707 4.293a1 1 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a1 1 0 1 0 1.414 1.414L10 11.414l4.293 4.293a1 1 0 1 0 1.414-1.414L11.414 10z"
              clipRule="evenodd"
            />
          </svg>
        </button>
      </div>
      <h2 className="text-center text-2xl font-bold mb-5">Thêm tài khoản</h2>

      <Form
        form={form}
        name="basic"
        labelCol={{
          span: 7,
        }}
        wrapperCol={{
          span: 16,
        }}
        className="max-w-md mx-auto"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        autoComplete="off"
      >
        <Form.Item
          label="Tên người dùng"
          name="name"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập tên người dùng!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Email"
          name="email"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập email!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Giới tính"
          name="gender"
          rules={[
            {
              required: true,
              message: "Vui lòng chọn giới tính!",
            },
          ]}
        >
          <Select>
            <Select.Option value="true">Nam</Select.Option>
            <Select.Option value="false">Nữ</Select.Option>
          </Select>
        </Form.Item>

        <Form.Item
          label="Mật khẩu"
          name="password"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập mật khẩu!",
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          label="Ngày sinh"
          name="birthday"
          rules={[
            {
              required: true,
              message: "Vui lòng chọn ngày sinh!",
            },
          ]}
        >
          <Input type="date" />
        </Form.Item>
        <Form.Item
          label="Quyền truy cập"
          name="role"
          rules={[
            {
              required: true,
              message: "Vui lòng chọn quyền truy cập!",
            },
          ]}
        >
          <Select>
            <Select.Option value="ADMIN">ADMIN</Select.Option>
            <Select.Option value="USER">USER</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 7, span: 16 }}>
          <button
            type="primary"
            htmlType="submit"
            className="  hover:bg-blue-800 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
            style={{ backgroundColor: "#35425e" }}
          >
            <span className="mx-auto">Submit</span>
          </button>
        </Form.Item>
      </Form>
    </div>
  );
}
