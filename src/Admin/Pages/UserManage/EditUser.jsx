import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getUserById, putUserById } from "../../redux/action/userManageAction";
import moment from "moment";
import { Form, Input, Select, Avatar, Divider } from "antd";
import { useParams } from "react-router-dom";
import { UserOutlined } from "@ant-design/icons";
import {
  FaFacebook,
  FaTwitter,
  FaInstagram,
  FaYoutube,
  FaPinterest,
} from "react-icons/fa";

export default function EditUser() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.userManageReducer.userDataById);

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [role, setRole] = useState("");
  const [birthday, setBirthday] = useState(null);
  const [gender, setGender] = useState("");
  const [editingMode, setEditingMode] = useState(false);

  useEffect(() => {
    dispatch(getUserById(parseInt(id)));
  }, [dispatch, id]);

  useEffect(() => {
    if (user) {
      setName(user.name);
      setEmail(user.email);
      setRole(user.role);
      setBirthday(user.birthday ? moment(user.birthday, "DD-MM-YYYY") : null);
      setGender(user.gender ? "Nam" : "Nữ");
    }
  }, [user]);

  const renderGender = () => {
    if (user.gender === true) {
      return "Nam";
    } else if (user.gender === false) {
      return "Nữ";
    } else {
      return "Không xác định";
    }
  };

  const handleSaveUser = () => {
    const formattedBirthday = birthday
      ? moment(birthday).format("DD-MM-YYYY")
      : null;
    const updatedUserData = {
      name,
      email,
      role,
      birthday: formattedBirthday,
      gender: gender === "Nam",
    };
    dispatch(putUserById(parseInt(id), updatedUserData));
    setEditingMode(false);
  };

  const renderEdit = () => (
    <div className="fixed inset-0 flex items-center justify-center bg-opacity-75 bg-gray-500">
      <div className="bg-white p-8 rounded " style={{ width: 500 }}>
        <h2 className="font-bold text-xl pb-5 text-center">
          Thay đổi thông tin
        </h2>
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          onFinish={handleSaveUser}
        >
          <Form.Item label="Tên người dùng">
            <Input value={name} onChange={(e) => setName(e.target.value)} />
          </Form.Item>
          <Form.Item label="Email">
            <Input value={email} onChange={(e) => setEmail(e.target.value)} />
          </Form.Item>
          <Form.Item label="Giới tính">
            <Select value={gender} onChange={(value) => setGender(value)}>
              <Select.Option value="Nam">Nam</Select.Option>
              <Select.Option value="Nữ">Nữ</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item label="Ngày sinh">
            <Input
              type="date"
              value={birthday}
              onChange={(e) => setBirthday(e.target.value)}
            />
          </Form.Item>
          <Form.Item label="Quyền truy cập">
            <Select value={role} onChange={(value) => setRole(value)}>
              <Select.Option value="ADMIN">ADMIN</Select.Option>
              <Select.Option value="USER">USER</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item className="flex justify-end">
            <div className="flex space-x-2">
              <div>
                <button
                  type="primary"
                  htmlType="submit"
                  className="    text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
                  style={{ backgroundColor: "#35425e" }}
                >
                  <span className="mx-auto">Lưu</span>
                </button>
              </div>
              <div>
                <button
                  type="primary"
                  className="   text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
                  style={{ backgroundColor: "#35425e" }}
                  onClick={() => setEditingMode(false)}
                >
                  Hủy
                </button>
              </div>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  );

  return (
    <div className="container mx-auto pt-5">
      <h1 className="pl-5 pb-5 text-3xl font-bold ">
        Thông tin tài khoản ( ID:{user.id} )
      </h1>
      <div className="px-10 py-10 m-5 border-solid border-2 border-black bg-gray-300">
        <div className="flex ">
          <div className="w-1/3 space-y-5 pr-10 ">
            <div className="w-full h-80 bg-white rounded flex flex-col items-center justify-center shadow">
              <Avatar
                size={150}
                src={user?.avatar}
                className="mb-4"
                icon={<UserOutlined />}
              />
              <div className="flex items-center py-1">
                <span className="text-xl font-semibold ">{user?.name}</span>
              </div>
              <div className="flex items-center py-1">
                <span className="text-lg font-semibold text-gray-500">
                  Quê hương: Việt Nam
                </span>
              </div>
            </div>
            <div className="w-full h-80 bg-white rounded px-10 items-center justify-center shadow">
              <div className="flex flex-col h-full justify-evenly">
                <div className="flex items-center flex justify-between ">
                  <div className="flex items-center">
                    <FaFacebook className="mr-3" />
                    <span className="font-semibold">Facebook</span>
                  </div>
                  <p>https://facebook.com</p>
                </div>
                <div className="flex items-center flex justify-between">
                  <div className="flex items-center ">
                    <FaTwitter className="mr-3" />
                    <span className="font-semibold">Twitter</span>
                  </div>
                  <p>https://twitter.com</p>
                </div>
                <div className="flex items-center flex justify-between">
                  <div className="flex items-center ">
                    <FaInstagram className="mr-3" />
                    <span className="font-semibold">Instagram</span>
                  </div>
                  <p>https://instagram.com</p>
                </div>
                <div className="flex items-center flex justify-between">
                  <div className="flex items-center ">
                    <FaYoutube className="mr-3" />
                    <span className="font-semibold">YouTube</span>
                  </div>
                  <p>https://youtube.com</p>
                </div>
                <div className="flex items-center flex justify-between">
                  <div className="flex items-center ">
                    <FaPinterest className="mr-3" />
                    <span className="font-semibold">Pinterest</span>
                  </div>
                  <p>https://pinterest.com</p>
                </div>
              </div>
            </div>
          </div>
          <div className="w-2/3 bg-white shadow rounded p-10">
            <div className="col-span-2">
              <div className="mb-6">
                <h2 className="text-xl font-semibold mb-2">
                  Chi tiết thông tin người dùng
                </h2>
                <Divider />
                <p className="pl-10">Email : {user?.email}</p>
                <Divider />
                <p className="pl-10">Ngày sinh : {user?.birthday}</p>
                <Divider />
                <p className="pl-10">Giới tính : {renderGender()}</p>
                <Divider />
                <p className="pl-10">Quyền truy cập : {user?.role}</p>
                <Divider />
              </div>
              {!editingMode && (
                <Form.Item wrapperCol={{ span: 4 }}>
                  <button
                    type="primary"
                    className="  hover:bg-blue-800 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
                    style={{ backgroundColor: "#35425e" }}
                    onClick={() => setEditingMode(true)}
                  >
                    <span className="mx-auto">Chỉnh sửa</span>
                  </button>
                </Form.Item>
              )}
              {editingMode && renderEdit()}
              <div>
                <h2 className="text-xl font-semibold mb-2">Giới thiệu</h2>
                <Divider />
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in
                  magna sed eros varius vulputate sed in nisl. Phasellus vitae
                  scelerisque velit, eu commodo magna. Aenean sed consectetur
                  ligula.Lorem ipsum dolor sit, amet consectetur adipisicing
                  elit.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
