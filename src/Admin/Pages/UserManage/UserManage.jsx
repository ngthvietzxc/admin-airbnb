import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import {
  deleteUser,
  getUser,
  searchUser,
} from "../../redux/action/userManageAction";
import moment from "moment";
import AddUser from "./AddUser";
import { Button, Input, Space } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import { NavLink } from "react-router-dom";
import { Pagination } from "@mui/material";
import { FiTrash2 } from "react-icons/fi";
export default function UserManage() {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.userManageReducer.userData);
  const [filterRole, setFilterRole] = useState("ADMIN");
  const filteredUsers = users.filter((user) => user.role === filterRole);
  const [isAdminList, setIsAdminList] = useState(true);

  const [searchKeyword, setSearchKeyword] = useState("");

  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = 8;
  const totalPages = Math.ceil(filteredUsers.length / itemsPerPage);
  const handlePageChange = (event, page) => {
    setCurrentPage(page);
  };
  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const currentPageUsers = filteredUsers.reverse().slice(startIndex, endIndex);

  useEffect(() => {
    dispatch(getUser());
  }, [dispatch]);

  const handleSearchChange = (e) => {
    const searchValue = e.target.value;
    setSearchKeyword(searchValue);

    if (searchValue === "") {
      dispatch(getUser());
    } else {
      dispatch(searchUser(searchValue));
    }
  };

  useEffect(() => {
    if (searchKeyword === "") {
      dispatch(getUser());
    }
  }, [dispatch, searchKeyword]);

  //Xóa User
  const handleDeleteUser = (id) => {
    dispatch(deleteUser(id));
  };
  // Add User
  const handleAddUserClick = () => {
    setShowAddUser(!showAddUser);
  };

  const [showAddUser, setShowAddUser] = useState(false);

  return (
    <div className="container mx-auto pt-5">
      <h1 className="pl-5 pb-5 text-3xl font-bold ">Quản lý người dùng</h1>
      <div className="flex space-x-3 py-3 pl-5">
        <div className="w-48 flex justify-center">
          <button
            className={`${
              isAdminList
                ? "text-black focus:outline-none focus:ring focus:ring-slate-400 bg-gray-200 "
                : "text-black"
            } font-semibold border rounded p-1 w-full`}
            onClick={() => {
              setFilterRole("ADMIN");
              setIsAdminList(true);
            }}
          >
            ADMIN
          </button>
        </div>
        <div className="w-48 flex justify-center">
          <button
            className={` ${
              !isAdminList
                ? "text-black border-b focus:outline-none focus:ring focus:ring-slate-400 bg-gray-200  "
                : " text-black "
            }
           font-semibold border rounded p-1 w-full `}
            onClick={() => {
              setFilterRole("USER");
              setIsAdminList(false);
            }}
          >
            USER
          </button>
        </div>
      </div>
      <div className="px-10 py-5 m-5 border-solid border-2 border-black bg-gray-300">
        <div className="flex pb-5 justify-between">
          <Space>
            <Input
              value={searchKeyword}
              onChange={handleSearchChange}
              type="text"
              className="w-96 py-1 font-semibold pr-10 pl-4 border border-gray-300 rounded-lg focus:outline-none focus:border-blue-500"
              placeholder="Tìm kiếm người dùng"
              prefix={<SearchOutlined />}
            />
          </Space>
          <div className="w-48">
            <Button
              type="primary"
              htmlType="submit"
              className="   hover:bg-blue-800 text-white font-semibold rounded px-4 py-2 w-full flex justify-center items-center"
              style={{ backgroundColor: "#35425e" }}
              onClick={handleAddUserClick}
            >
              <span className="mx-auto">Thêm tài khoản</span>
            </Button>
          </div>
        </div>
        <p className="text-center pb-5 text-gray-800 text-xl font-bold">
          {isAdminList ? "DANH SÁCH ADMIN" : "DANH SÁCH USER"}
        </p>
        <table className="min-w-full border border-gray-300 drop-shadow">
          <thead className="bg-gray-500 text-white">
            <tr>
              <th className="py-2 px-3 border-r w-16 font-semibold">ID</th>
              <th className="py-2 px-3 border-r w-96 font-semibold">
                Họ Và Tên
              </th>
              <th className="py-2 px-3 border-r w-96 font-semibold">Email</th>
              <th className="py-2 px-3 border-r w-48 font-semibold">
                Ngày Sinh
              </th>
              <th className="py-2 px-3 border-r w-36 font-semibold">
                Giới tính
              </th>
              <th className="py-2 px-3 font-semibold">Actions</th>
            </tr>
          </thead>
          <tbody>
            {currentPageUsers.map((user) => (
              <tr key={user.id} className="border-b">
                <td className="py-2 bg-white  px-4 border-r">{user.id}</td>
                <td className="py-2 bg-white  px-10 border-r">{user.name}</td>
                <td className="py-2 bg-white  px-10 border-r">{user.email}</td>
                <td className="py-2 bg-white  px-10 border-r">
                  {user.birthday
                    ? moment(user.birthday, "YYYY/MM/DD").format("DD/MM/YYYY")
                    : "-"}
                </td>
                <td className="py-2 bg-white  px-10 border-r">
                  {user.gender ? "Nam" : "Nữ"}
                </td>
                <td className="py-2 bg-white  px-10 space-x-5 flex ">
                  <div className="w-24 h-8">
                    <NavLink
                      to={`/admin/users/${user.id}`}
                      type="warning"
                      className=" text-white font-semibold text-xs rounded   w-full h-full flex justify-center items-center"
                      style={{ backgroundColor: "#35425e" }}
                    >
                      <span className="mx-auto">Chi tiết</span>
                    </NavLink>
                  </div>
                  <div className="w-10 h-8">
                    <button
                      className="bg-red-600 text-white text-lg justify-center flex rounded h-full w-full flex  items-center"
                      onClick={() => handleDeleteUser(user.id)}
                    >
                      <FiTrash2 />
                    </button>
                  </div>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <div className="flex justify-center pt-7 pb-3">
          <Pagination
            variant="outlined"
            color="primary"
            count={totalPages}
            page={currentPage}
            onChange={handlePageChange}
          />
        </div>
      </div>
      {showAddUser && (
        <div className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50">
          <div className="bg-white p-8 rounded-lg">
            <AddUser onClose={handleAddUserClick} />
          </div>
        </div>
      )}
    </div>
  );
}
