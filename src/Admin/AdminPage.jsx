import React from "react";
import { Route, Routes } from "react-router-dom";
import SideBar from "./Components/SideBar/SideBar";
import sidebar_menu from "./redux/constant/SlideBarConstant";
import UserManage from "./Pages/UserManage/UserManage";
import Header from "./Components/Header/Header";
import EditUser from "./Pages/UserManage/EditUser";
import LocationManage from "./Pages/LocationManage/LocationManage";
import RoomManage from "./Pages/RoomManage/RoomManage";
import BookingManage from "./Pages/BookingManage/BookingManage";
import EditBooking from "./Pages/BookingManage/EditBooking";
import EditLocation from "./Pages/LocationManage/EditLocation";
import EditRoom from "./Pages/RoomManage/EditRoom";
import Dashboard from "./Pages/Dashboard/Dashboard";
import Login from "./Login/Login";

export default function AdminPage() {
  return (
    <div className="h-screen">
      <div className="flex flex-row">
        <SideBar menu={sidebar_menu} />
        <div className="flex-column flex-1">
          <Header />
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/users" element={<UserManage />} />
            <Route path="/users/:id" element={<EditUser />} />
            <Route path="/locations" element={<LocationManage />} />
            <Route path="/locations/:id" element={<EditLocation />} />
            <Route path="/rooms" element={<RoomManage />} />
            <Route path="/rooms/:id" element={<EditRoom />} />
            <Route path="/bookings" element={<BookingManage />} />
            <Route path="/bookings/:id" element={<EditBooking />} />
          </Routes>
        </div>
      </div>
    </div>
  );
}
