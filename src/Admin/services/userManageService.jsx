import axios from "axios";
import { BASE_URL, configHeaders } from "./config";
import { localUserService } from "./localService";

export const userManageService = {
  getUser: () => {
    return axios({
      url: `${BASE_URL}/api/users`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  searchUser: (keyword) => {
    return axios({
      url: `${BASE_URL}/api/users/search/${keyword}`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  addUser: (userAdd) => {
    return axios({
      url: `${BASE_URL}/api/users`,
      method: "POST",
      data: userAdd,
      headers: configHeaders(),
    });
  },
  deleteUser: (id) => {
    return axios({
      url: `${BASE_URL}/api/users?id=${id}`,
      method: "DELETE",
      headers: configHeaders(),
    });
  },
  getUserById: (id) => {
    return axios({
      url: `${BASE_URL}/api/users/${id}`,
      method: "GET",
      headers: configHeaders(),
    });
  },
  putUserById: (id, userDataById) => {
    return axios({
      url: `${BASE_URL}/api/users/${id}`,
      method: "PUT",
      data: userDataById,
      headers: configHeaders(),
    });
  },
  postAvatar: (avatarFile) => {
    const token = localUserService.get()?.token;
    const formData = new FormData();
    formData.append("formFile", avatarFile);

    return axios({
      url: `${BASE_URL}/api/users/upload-avatar`,
      method: "POST",
      data: formData,
      headers: {
        ...configHeaders(),
        token: token,
        "Content-Type": "multipart/form-data",
      },
    });
  },
};
