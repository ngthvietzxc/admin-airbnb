import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Avatar, Button, Popover } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import { localUserService } from "../../services/localService";
import { AiFillCaretDown } from "react-icons/ai";

import { postAvatar } from "./../../redux/action/userManageAction";

export default function Header() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const userInfo = useSelector((state) => {
    return state.userReducer.userInfo;
  });

  const [isPopoverVisible, setPopoverVisible] = useState(false);

  const handlePopoverVisibleChange = (visible) => {
    setPopoverVisible(visible);
  };

  const handleLogout = () => {
    localUserService.remove();
    navigate("/");
  };

  const handleAvatarChange = (e) => {
    const file = e.target.files[0];
    if (file) {
      dispatch(postAvatar(file));
    }
  };

  return (
    <div className="flex items-center justify-end h-16  bg-black ">
      <div className="flex items-center pr-16 border-r border-white ">
        {userInfo.user && (
          <div className="relative">
            <Popover
              content={
                <div className="flex flex-col">
                  <NavLink
                    to={`/admin/users/${userInfo.user.id}`}
                    className="text-black font-semibold hover:underline"
                    onClick={() => {
                      handlePopoverVisibleChange(false);
                    }}
                  >
                    <Button type="text" className="text-black font-semibold">
                      Xem thông tin tài khoản
                    </Button>
                  </NavLink>
                  <Button
                    type="text"
                    className="text-black font-semibold ant-btn-link"
                    onClick={() => {
                      // Open the file input on button click
                      document.getElementById("avatar-input").click();
                      handlePopoverVisibleChange(false);
                    }}
                  >
                    Thay đổi Avatar
                  </Button>
                  <Button
                    type="text"
                    className="text-black font-semibold ant-btn-link"
                    onClick={handleLogout}
                  >
                    Đăng xuất
                  </Button>
                  <input
                    id="avatar-input"
                    type="file"
                    accept="image/*"
                    style={{ display: "none" }}
                    onChange={handleAvatarChange}
                  />
                </div>
              }
              trigger="click"
              open={isPopoverVisible}
              onOpenChange={handlePopoverVisibleChange}
            >
              <div className="flex items-center cursor-pointer">
                <Avatar
                  size="large"
                  src={userInfo.user.avatar}
                  className="mr-2"
                />
                <span className="text-white font-semibold">
                  {userInfo.user.name}
                </span>
                <div className="pl-3">
                  <AiFillCaretDown className="text-white" />
                </div>
              </div>
            </Popover>
          </div>
        )}
      </div>
    </div>
  );
}
