import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import SideBarItem from "./SideBarItem";
import "./SideBar.css";
import { FiLogOut } from "react-icons/fi";
import { localUserService } from "../../services/localService";

function SideBar({ menu }) {
  const location = useLocation();
  const [active, setActive] = useState(1);
  const navigate = useNavigate();

  useEffect(() => {
    menu.forEach((element) => {
      if (location.pathname === element.path) {
        setActive(element.id);
      }
    });
  }, [location.pathname, menu]);

  const __navigate = (id) => {
    setActive(id);
  };

  const handleLogout = () => {
    localUserService.remove();
    navigate("/");
  };

  return (
    <nav className="sidebar">
      <div className="sidebar-container">
        <div className="sidebar-items">
          {menu.map((item, index) => (
            <div key={index} onClick={() => __navigate(item.id)}>
              <SideBarItem active={item.id === active} item={item} />
            </div>
          ))}
        </div>

        <div className="sidebar-footer" onClick={handleLogout}>
          <span className="sidebar-item-icon">
            <FiLogOut />
          </span>
          <span className="sidebar-item-label">Logout</span>
        </div>
      </div>
    </nav>
  );
}

export default SideBar;
