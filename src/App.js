import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import AdminPage from "./Admin/AdminPage";
import Login from "./Admin/Login/Login";

function App() {
  return (
    <div>
      <Router>
        <Routes>
          <Route path="/admin/*" element={<AdminPage />} />
          <Route path="/" element={<Login />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
